var path = require('path');

var settings = {
    srcPath: './src/Klumb/front/',
    distPath: './dist/',
    publicPath: '/dist/js/'
};

module.exports = {
    entry: {
        app:  settings.srcPath + 'js/app.js'
    },
    output: {
        path: settings.distPath + 'js',
        filename: '[name].js',
        publicPath: settings.publicPath
    },
    module: {},
    resolve: {
        root: [
            path.resolve(settings.srcPath)
        ],
        alias: {
            'Backbone': require.resolve('backbone')
        },
        modulesDirectories: ['node_modules']
    },

};
