module.exports = function(grunt) {

    var environment = grunt.option('env') || 'development';

    grunt.initConfig({

        settings: {
            rootPath: '',
            srcPath: 'src/Klumb/front/',
            distPath: 'dist/'
        },

        sass: {
            app: {
                files: [{
                    expand: true,
                    cwd: '<%= settings.srcPath %>scss',
                    src: ['**/*.scss'],
                    dest: '<%= settings.distPath %>css',
                    ext: '.css'
                }],
                options: {
                    outputStyle: environment === 'development' ? 'expanded' : 'compressed',
                    sourceMap: false,
                    precision: 5,
                    includePaths: [
                        'node_modules'
                    ]
                }
            }
        },

        sync: {},

        jshint: {
            options: {
                jshintrc: '<%= settings.rootPath %>.jshintrc'
            },
            files: {
                src: ['<%= settings.srcPath %>js/**/*.js', 'Gruntfile.js']
            }
        },

        jscs: {
            options: {
                config: '<%= settings.rootPath %>.jscsrc'
            },
            scripts: {
                files: {
                    src: ['<%= settings.srcPath %>js/**/*.js', 'Gruntfile.js']
                }
            }
        },

        watch: {
            javascript: {
                expand: true,
                files: ['<%= settings.srcPath %>js/**/*.js', 'Gruntfile.js'],
                tasks: [/*'jshint', 'jscs'*/],
                options: {
                    spawn: false
                }
            },
            scss: {
                expand: true,
                files: ['<%= settings.srcPath %>scss/**/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false
                }
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', [/*'jshint', 'jscs',*/ 'sass']);

};
