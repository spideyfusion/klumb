<?php
require_once __DIR__ . '/vendor/autoload.php';

$app = new Silex\Application();

$app['debug'] = true;
$app->register(new Silex\Provider\ServiceControllerServiceProvider());

$app->register(new Silex\Provider\DoctrineServiceProvider(), [
    'db.options' => [
        'driver'   => 'pdo_mysql',
        'dbname'   => 'klumb',
        'password' => 'wogibtswas',
        'host' => '192.168.1.30'

    ],
]);

$app['controller.index'] = $app->share(function() use ($app) {
    return new Klumb\Controller\IndexController($app);
});

$app['controller.location'] = $app->share(function() use ($app) {
    return new Klumb\Controller\LocationController($app);
});


$app['controller.login'] = $app->share(function() use ($app) {
    return new Klumb\Controller\LoginController($app);
});

$app['template.path'] = __DIR__ . '/front';

$app->get('/', 'controller.index:indexAction');
$app->get('/location', 'controller.location:getLocationAction');
$app->post('/location', 'controller.location:setLocationAction');
$app->post('/login', 'controller.login:loginAction');

$app->run();


