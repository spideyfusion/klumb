<?php
namespace Klumb\Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController
{
	private $app;

	public function __construct($app)
	{
		$this->app = $app;
	}

	public function indexAction()
	{
		$template = file_get_contents($this->app['template.path'] . '/main.html');

		return new Response($template);
	}
}
