<?php


namespace Klumb\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LoginController
{

    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }


    public function loginAction()
    {
        $request = Request::createFromGlobals();
        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $db = $this->app['db'];
        // @todo : hash password
//        $password = sha1($password);

        $user = $this->fetchUser($username, $password);

        if ($user != false){
            return new JsonResponse(['login' => true,'type' => $user['type']]);
        }

        return new JsonResponse(['login' => false]);
    }

    /**
     * @param $username
     * @param $password
     * @return mixed
     */
    protected function fetchUser($username, $password)
    {
        $user = $this->app['db']->fetchAssoc('SELECT * FROM user where name = ? AND password = ?',
            array($username, $password));

        return $user;

    }


}