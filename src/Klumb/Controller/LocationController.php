<?php


namespace Klumb\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LocationController
{

    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }


    public function getLocationAction()
    {
        $location = $this->app['db']->fetchAll
        ('SELECT * FROM location ORDER BY created_ts DESC LIMIT 1');
        return new JsonResponse(['latitude' => $location[0]['latitude'], 'longitude' => $location[0]['longitude']]);
    }

    public function setLocationAction()
    {
        $request = Request::createFromGlobals();
        $latitude = $request->request->get('latitude');
        $longitude = $request->request->get('longitude');
        $this->saveLocation($latitude, $longitude);
        return new JsonResponse(['latitude' => $latitude, 'longitude' => $longitude]);
    }

    /**
     * @param $latitude
     * @param $longitude
     */
    protected function saveLocation($latitude, $longitude)
    {
        $time = new \DateTime('now');
        $this->app['db']->executeQuery('insert into location (latitude, longitude,created_ts) values(?, ?,?)',
            [$latitude, $longitude, $time->format('Y-m-d H:i:s')]);
    }
}