var $ = require('jquery');
var Backbone = require('backbone');
var Login = require('js/controller/login.js');
var Client = require('js/controller/client.js');
var Find = require('js/controller/find.js');
var Ping = require('js/controller/ping.js');
var Kile = require('js/controller/kile.js');

var AppRouter = Backbone.Router.extend({
    
    routes: {
        'login': 'login',
        'client': 'client',
        'find': 'find',
        'ping': 'ping',
        'kile': 'kile'
    },

    login: function(){

        var view = new Login({
          router: this
        });
        $("#app").html(view.$el);

    },

    client: function(){

        var view = new Client({
          router: this
        });
        $("#app").html(view.$el);

    },

    find: function(){

        var view = new Find({
          router: this
        });
        $("#app").html(view.$el);

    },

    ping: function(){

        var view = new Ping({
          router: this
        });
        $("#app").html(view.$el);

    },

    kile: function(){

        var view = new Kile({
          router: this
        });
        $("#app").html(view.$el);

    }

});

 var ApplicationView = Backbone.View.extend({

  el: $('#app'),

  events: {},

  initialize: function(){

      this.router = new AppRouter();
      Backbone.history.start();

      this.router.navigate('login', true);

  }

});


module.exports = (function() {

    this.app = new ApplicationView();

})();
