var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');

module.exports = Backbone.View.extend({

    initialize: function(options) {

        this.router = options.router;

        this.render();

    },

    events: {

        'click .find': 'findKile',
        'click .ping': 'pingKile',
        'click .logoutBtn': 'logout'

    },

    template: _.template('<div class="barTop"><h1>Where is Kile?</h1>' +
        '<button class="logoutBtn">Log Out</button>' + '</div>' +
        '<div class="welcomeText">Hi, Davor</div>' +
        '<div class="welcomeParagraf">Choose your mission</div>' +
        '<button class="find">Find Kile</button>' +
        '<button class="ping">Ping Kile</button>'),

    render: function() {

        this.$el.html(this.template());

    },

    findKile: function() {

        this.router.navigate('find', true);

    },

    pingKile: function() {

        this.router.navigate('ping', true);

    },

    logout: function() {

        this.router.navigate('login', true);

    }

});
