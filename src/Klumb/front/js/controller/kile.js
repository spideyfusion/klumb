var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');

module.exports = Backbone.View.extend({

    initialize: function(options) {

        this.router = options.router;

        this.render();

    },

    events: {

        'click .logoutBtn': 'logout',
        'click .setLocation': 'setLocation'

    },

    template: _.template('<div class="barTop"><h1>Where is Kile?</h1>' +
        '<button class="logoutBtn">Log Out</button>' + '</div>' +
        '<div class="welcomeText">Hi, Kile</div>' +
        '<button class="setLocation">Set my location!</button>' +
        '<ul class="listHelp">' +
            '<li>Upit upomoc 1</li>' +
            '<li>Upit upomoc 2</li>' +
            '<li>Upit upomoc 3</li>' +
            '<li>Upit upomoc 4</li>' +
        '<ul>'),

    render: function() {

        this.$el.html(this.template());

    },

    setLocation: function() {

        $.ajax({

            type: 'POST',
            url: '/location',
            data: {
                latitude: 45.798323,
                longitude: 15.9694728
            }

        }).done(function(data) {

           alert('Location is set!');

        });

    },

    logout: function() {

        this.router.navigate('login', true);

    }

});
