var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');

module.exports = Backbone.View.extend({

    initialize: function(options) {

        this.router = options.router;

        this.render();

    },

    events: {
        'submit #submitForm': 'onLogin'
    },

    template: _.template('<div class="barTop"><h1>Where is Kile?</h1></div>' +
        '<div class="welcomeText">Welcome</div>' +
        '<form id="submitForm">' +
            '<label>Username <input type="text" name="username" require/></label>' +
            '<label>password <input type="password" name="password" require /></label>' +
            '<button type="submit">Submit</button>' +
        '</form>'),

    render: function() {

        this.$el.html(this.template());

    },

    onLogin: function(e) {

        e.preventDefault();

        var $form = $(e.currentTarget);

        console.log($form.serialize());

        $.post({
            
            type: 'POST',
            url: '/login',
            data: $form.serialize()

        }).done(function(data){

            if(data.login){

                if(data.type == 1){
                    this.router.navigate('kile', true);
                } else {
                    this.router.navigate('client', true);
                }

            } else {

                alert('Wrong credentials!');

            }

        }.bind(this)).fail(function(data){

            alert('Error has occured!');

        }.bind(this));

    }
});
