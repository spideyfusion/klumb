var $ = require('jquery');
var Backbone = require('backbone');
var _ = require('underscore');


module.exports = Backbone.View.extend({

    initialize: function(options) {


        this.router = options.router;

        this.render();

    },

    events: {

        'click .logoutBtn': 'logout',
        'click .backBtn': 'backBtn'

    },

    template: _.template('<div class="barTop"><h1>Where is Kile?</h1>' +
        '<button class="backBtn">Go Back</button>' +
        '<button class="logoutBtn">Log Out</button>' + '</div>' +
        '<div id="map"></div>'),

    render: function() {

        this.$el.html(this.template());

        $.ajax({
            
            type: 'GET',
            url: '/location'

        }).done(function(data) {

            var myLatLng = {lat: parseFloat(data.latitude), lng: parseFloat(data.longitude)};

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 18,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Here is KILE!'
            });


        });

    },

    logout: function() {

        this.router.navigate('login', true);

    },

    backBtn: function() {

        this.router.navigate('client', true);

    }

});
